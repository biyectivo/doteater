{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Space Marine",
  "styleName": "Nominal",
  "size": 20.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 1,
  "glyphs": {
    "32": {"x":2,"y":2,"w":10,"h":27,"character":32,"shift":10,"offset":0,},
    "33": {"x":482,"y":60,"w":8,"h":27,"character":33,"shift":10,"offset":1,},
    "34": {"x":470,"y":60,"w":10,"h":27,"character":34,"shift":16,"offset":3,},
    "35": {"x":441,"y":60,"w":27,"h":27,"character":35,"shift":27,"offset":0,},
    "36": {"x":427,"y":60,"w":12,"h":27,"character":36,"shift":16,"offset":2,},
    "37": {"x":408,"y":60,"w":17,"h":27,"character":37,"shift":16,"offset":0,},
    "38": {"x":389,"y":60,"w":17,"h":27,"character":38,"shift":16,"offset":0,},
    "39": {"x":383,"y":60,"w":4,"h":27,"character":39,"shift":16,"offset":6,},
    "40": {"x":371,"y":60,"w":10,"h":27,"character":40,"shift":16,"offset":3,},
    "41": {"x":359,"y":60,"w":10,"h":27,"character":41,"shift":16,"offset":3,},
    "42": {"x":492,"y":60,"w":14,"h":27,"character":42,"shift":16,"offset":1,},
    "43": {"x":344,"y":60,"w":13,"h":27,"character":43,"shift":16,"offset":2,},
    "44": {"x":308,"y":60,"w":8,"h":27,"character":44,"shift":10,"offset":1,},
    "45": {"x":296,"y":60,"w":10,"h":27,"character":45,"shift":16,"offset":3,},
    "46": {"x":286,"y":60,"w":8,"h":27,"character":46,"shift":10,"offset":1,},
    "47": {"x":272,"y":60,"w":12,"h":27,"character":47,"shift":16,"offset":2,},
    "48": {"x":246,"y":60,"w":24,"h":27,"character":48,"shift":27,"offset":1,},
    "49": {"x":236,"y":60,"w":8,"h":27,"character":49,"shift":10,"offset":1,},
    "50": {"x":210,"y":60,"w":24,"h":27,"character":50,"shift":27,"offset":1,},
    "51": {"x":184,"y":60,"w":24,"h":27,"character":51,"shift":27,"offset":1,},
    "52": {"x":158,"y":60,"w":24,"h":27,"character":52,"shift":27,"offset":1,},
    "53": {"x":318,"y":60,"w":24,"h":27,"character":53,"shift":27,"offset":1,},
    "54": {"x":2,"y":89,"w":24,"h":27,"character":54,"shift":27,"offset":1,},
    "55": {"x":28,"y":89,"w":24,"h":27,"character":55,"shift":27,"offset":1,},
    "56": {"x":54,"y":89,"w":24,"h":27,"character":56,"shift":27,"offset":1,},
    "57": {"x":2,"y":118,"w":24,"h":27,"character":57,"shift":27,"offset":1,},
    "58": {"x":499,"y":89,"w":8,"h":27,"character":58,"shift":10,"offset":1,},
    "59": {"x":489,"y":89,"w":8,"h":27,"character":59,"shift":10,"offset":1,},
    "60": {"x":474,"y":89,"w":13,"h":27,"character":60,"shift":16,"offset":2,},
    "61": {"x":459,"y":89,"w":13,"h":27,"character":61,"shift":16,"offset":2,},
    "62": {"x":444,"y":89,"w":13,"h":27,"character":62,"shift":16,"offset":2,},
    "63": {"x":424,"y":89,"w":18,"h":27,"character":63,"shift":20,"offset":1,},
    "64": {"x":406,"y":89,"w":16,"h":27,"character":64,"shift":16,"offset":0,},
    "65": {"x":380,"y":89,"w":24,"h":27,"character":65,"shift":27,"offset":1,},
    "66": {"x":354,"y":89,"w":24,"h":27,"character":66,"shift":27,"offset":1,},
    "67": {"x":328,"y":89,"w":24,"h":27,"character":67,"shift":27,"offset":1,},
    "68": {"x":302,"y":89,"w":24,"h":27,"character":68,"shift":27,"offset":1,},
    "69": {"x":276,"y":89,"w":24,"h":27,"character":69,"shift":27,"offset":1,},
    "70": {"x":250,"y":89,"w":24,"h":27,"character":70,"shift":27,"offset":1,},
    "71": {"x":224,"y":89,"w":24,"h":27,"character":71,"shift":27,"offset":1,},
    "72": {"x":198,"y":89,"w":24,"h":27,"character":72,"shift":27,"offset":1,},
    "73": {"x":188,"y":89,"w":8,"h":27,"character":73,"shift":10,"offset":1,},
    "74": {"x":162,"y":89,"w":24,"h":27,"character":74,"shift":27,"offset":1,},
    "75": {"x":136,"y":89,"w":24,"h":27,"character":75,"shift":27,"offset":1,},
    "76": {"x":110,"y":89,"w":24,"h":27,"character":76,"shift":27,"offset":1,},
    "77": {"x":80,"y":89,"w":28,"h":27,"character":77,"shift":30,"offset":1,},
    "78": {"x":132,"y":60,"w":24,"h":27,"character":78,"shift":27,"offset":1,},
    "79": {"x":106,"y":60,"w":24,"h":27,"character":79,"shift":27,"offset":1,},
    "80": {"x":80,"y":60,"w":24,"h":27,"character":80,"shift":27,"offset":1,},
    "81": {"x":2,"y":31,"w":24,"h":27,"character":81,"shift":27,"offset":1,},
    "82": {"x":452,"y":2,"w":24,"h":27,"character":82,"shift":27,"offset":1,},
    "83": {"x":426,"y":2,"w":24,"h":27,"character":83,"shift":27,"offset":1,},
    "84": {"x":400,"y":2,"w":24,"h":27,"character":84,"shift":27,"offset":1,},
    "85": {"x":374,"y":2,"w":24,"h":27,"character":85,"shift":27,"offset":1,},
    "86": {"x":348,"y":2,"w":24,"h":27,"character":86,"shift":27,"offset":1,},
    "87": {"x":318,"y":2,"w":28,"h":27,"character":87,"shift":30,"offset":1,},
    "88": {"x":292,"y":2,"w":24,"h":27,"character":88,"shift":27,"offset":1,},
    "89": {"x":266,"y":2,"w":24,"h":27,"character":89,"shift":27,"offset":1,},
    "90": {"x":240,"y":2,"w":24,"h":27,"character":90,"shift":27,"offset":1,},
    "91": {"x":478,"y":2,"w":8,"h":27,"character":91,"shift":16,"offset":5,},
    "92": {"x":226,"y":2,"w":12,"h":27,"character":92,"shift":16,"offset":2,},
    "93": {"x":190,"y":2,"w":8,"h":27,"character":93,"shift":16,"offset":3,},
    "94": {"x":174,"y":2,"w":14,"h":27,"character":94,"shift":16,"offset":1,},
    "95": {"x":154,"y":2,"w":18,"h":27,"character":95,"shift":16,"offset":-1,},
    "96": {"x":144,"y":2,"w":8,"h":27,"character":96,"shift":10,"offset":1,},
    "97": {"x":118,"y":2,"w":24,"h":27,"character":97,"shift":27,"offset":1,},
    "98": {"x":92,"y":2,"w":24,"h":27,"character":98,"shift":27,"offset":1,},
    "99": {"x":66,"y":2,"w":24,"h":27,"character":99,"shift":27,"offset":1,},
    "100": {"x":40,"y":2,"w":24,"h":27,"character":100,"shift":27,"offset":1,},
    "101": {"x":14,"y":2,"w":24,"h":27,"character":101,"shift":27,"offset":1,},
    "102": {"x":200,"y":2,"w":24,"h":27,"character":102,"shift":27,"offset":1,},
    "103": {"x":28,"y":31,"w":24,"h":27,"character":103,"shift":27,"offset":1,},
    "104": {"x":256,"y":31,"w":24,"h":27,"character":104,"shift":27,"offset":1,},
    "105": {"x":54,"y":31,"w":8,"h":27,"character":105,"shift":10,"offset":1,},
    "106": {"x":28,"y":60,"w":24,"h":27,"character":106,"shift":27,"offset":1,},
    "107": {"x":2,"y":60,"w":24,"h":27,"character":107,"shift":27,"offset":1,},
    "108": {"x":468,"y":31,"w":24,"h":27,"character":108,"shift":27,"offset":1,},
    "109": {"x":438,"y":31,"w":28,"h":27,"character":109,"shift":30,"offset":1,},
    "110": {"x":412,"y":31,"w":24,"h":27,"character":110,"shift":27,"offset":1,},
    "111": {"x":386,"y":31,"w":24,"h":27,"character":111,"shift":27,"offset":1,},
    "112": {"x":360,"y":31,"w":24,"h":27,"character":112,"shift":27,"offset":1,},
    "113": {"x":334,"y":31,"w":24,"h":27,"character":113,"shift":27,"offset":1,},
    "114": {"x":308,"y":31,"w":24,"h":27,"character":114,"shift":27,"offset":1,},
    "115": {"x":54,"y":60,"w":24,"h":27,"character":115,"shift":27,"offset":1,},
    "116": {"x":282,"y":31,"w":24,"h":27,"character":116,"shift":27,"offset":1,},
    "117": {"x":230,"y":31,"w":24,"h":27,"character":117,"shift":27,"offset":1,},
    "118": {"x":204,"y":31,"w":24,"h":27,"character":118,"shift":27,"offset":1,},
    "119": {"x":174,"y":31,"w":28,"h":27,"character":119,"shift":30,"offset":1,},
    "120": {"x":148,"y":31,"w":24,"h":27,"character":120,"shift":27,"offset":1,},
    "121": {"x":122,"y":31,"w":24,"h":27,"character":121,"shift":27,"offset":1,},
    "122": {"x":96,"y":31,"w":24,"h":27,"character":122,"shift":27,"offset":1,},
    "123": {"x":82,"y":31,"w":12,"h":27,"character":123,"shift":16,"offset":2,},
    "124": {"x":77,"y":31,"w":3,"h":27,"character":124,"shift":16,"offset":7,},
    "125": {"x":64,"y":31,"w":11,"h":27,"character":125,"shift":16,"offset":3,},
    "126": {"x":28,"y":118,"w":13,"h":27,"character":126,"shift":16,"offset":2,},
    "9647": {"x":43,"y":118,"w":16,"h":27,"character":9647,"shift":26,"offset":5,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":255,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_Menu",
  "tags": [],
  "resourceType": "GMFont",
}