fnc_BackupDrawParams();
if (room != destination_room) {
	//view_picture = sprite_create_from_surface(application_surface, 0, 0, surface_get_width(application_surface), surface_get_height(application_surface), false, false, 0, 0);
	screen_save_part("_tmp_screenshot_1.png", 0, 0, window_get_width(), window_get_height());
	view_picture = sprite_add("_tmp_screenshot_1.png", 0, false, false, 0, 0);

	if (room_exists(destination_room) && room != destination_room) {
		room_goto(destination_room);
	}
}
else {
	if (transition_type == TRANSITION.FADE_OUT) {
		var _channel = animcurve_get_channel(CSSTransitions, "ease-out");
		var _alpha = 1-animcurve_channel_evaluate(_channel, 1-alarm[0]/max_time);
		if (sprite_exists(view_picture)) {
			draw_sprite_ext(view_picture, 0, 0, 0, window_get_width()/sprite_get_width(view_picture), window_get_height()/sprite_get_height(view_picture), 0, c_white, _alpha);
		}
	}	
}
fnc_RestoreDrawParams();
