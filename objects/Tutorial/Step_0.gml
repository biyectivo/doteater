/// @description 
if ( (current_message == 0 && keyboard_check_pressed(vk_f1)) || (current_message > 0 && alarm[0] == -1)) { // Start tutorial
	draw_message = true;
}


if (modal && current_message<array_length(tutorial) && !tutorial[current_message].is_dismissed && (keyboard_check_pressed(progress_key) || device_mouse_check_button_pressed(0, progress_mouse) || gamepad_button_check_pressed(Game.primary_gamepad, progress_button))) {
	tutorial[current_message].is_dismissed = true;
	obj_Player.controllable = true;
	current_message++;
}