/// @description 

// To continue a paused message:
// Tutorial.tutorial[Tutorial.current_message].is_paused = false; 
// instance_destroy();

TutorialMessage = function(_title, _text, _paused, _duration) constructor {
	title = _title;
	text = _text;
	is_paused = _paused;
	is_dismissed = false;
	duration = _duration;
};

tutorial = [];

current_message = 0;
modal = true;
progress_key = vk_enter;
progress_mouse = mb_left;
progress_button = gp_face1;

draw_message = false;

// Tutorial data
array_push(tutorial, new TutorialMessage("", "[c_white][fnt_Menu][fa_center]Message 1", false, 60*3));
array_push(tutorial, new TutorialMessage("", "[c_white][fnt_Menu][fa_center]Message 2", false, 60*2));
array_push(tutorial, new TutorialMessage("", "[c_white][fnt_Menu][fa_center]Message 3 (paused)", true, 60*5));
