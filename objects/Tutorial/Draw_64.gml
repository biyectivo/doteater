/// @description 
if (draw_message && current_message < array_length(tutorial)) {
	var _msg = tutorial[current_message];
	if (!_msg.is_paused) {
		if (modal) {
			if (!_msg.is_dismissed) {
				obj_Player.controllable = false;
				draw_rectangle_color(0, display_get_gui_height()-80, display_get_gui_width(), display_get_gui_height(), c_dkgray, c_dkgray, c_dkgray, c_dkgray, false);
				scribble(_msg.text).draw(display_get_gui_width()/2, display_get_gui_height()-60);
				scribble("[fnt_Debug][fa_center]Press ENTER or LMB to continue...").draw(display_get_gui_width()/2, display_get_gui_height()-20);
			}
		}
		else {
			draw_rectangle_color(0, display_get_gui_height()-80, display_get_gui_width(), display_get_gui_height(), c_dkgray, c_dkgray, c_dkgray, c_dkgray, false);
			scribble(_msg.text).draw(display_get_gui_width()/2, display_get_gui_height()-60);
			if (alarm[0] == -1) {
				alarm[0] = _msg.duration;		
			}
		}
		
	}
}