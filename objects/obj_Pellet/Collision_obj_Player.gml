/// @description 
if (!Game.paused && instantiated) {	
	if (alarm[0] == -1) {
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Pellet, 1, false);
		}
		Game.pellet_active = true;
		Game.alarm[4] = 60*8;
		show_debug_message("Pellet restarted");
		Game.total_score = Game.total_score + 50;
		alarm[0] = 60;
		if (random(1) < 0.5) {
			Game.camera_shake = true;
			Game.alarm[1] = SCREENSHAKE_DURATION;
			Game.num_ghosts++;
			show_debug_message("Create ghost, side effect!!!");
			var _id = instance_create_layer(choose(18,20)*TILE_SIZE+TILE_SIZE/2, choose(14,15)*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Ghost);
			with (_id) {
				ghost_color_red = choose(0,1);
				ghost_color_green = choose(0,1);
				ghost_color_blue = choose(0,1);
				path_type = choose(PATH_TYPE.CHASE, PATH_TYPE.GET_AHEAD);
				event_perform(ev_other, ev_user0);
			}
		}
	}
}