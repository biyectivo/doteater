/// @description 
if (!Game.paused && instantiated) {	
	if (alarm[0] == -1) {
		draw_self();	
	}
	else {
		scribble("[fa_center][fa_middle][fnt_MiniText][scale,0.75][c_white]50").blend(c_white, alarm[0]/60).draw(x,y);		
	}
}