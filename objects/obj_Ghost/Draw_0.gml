/// @description 

if (!Game.paused && instantiated) {		

	if (alarm[2] >= 0) { // Score
		scribble("[fa_center][fa_middle][fnt_MiniText][scale,1][c_white]"+string(Game.num_ghosts_eaten_this_pellet * 200)).blend(c_white, alarm[2]/60).draw(x,y);
	}	
	else if (Game.pellet_active && Game.alarm[4] <= 60*2) {		
		var _offset = 8*(floor( (60*2 - Game.alarm[4])/40 ) % 2);
		image_index = image_index + _offset;
		draw_self();
	}
	else {
		shader_set(shd_ReplaceColor);

		var _colorIn = shader_get_uniform(shd_ReplaceColor, "colorIn");
		var _colorOut = shader_get_uniform(shd_ReplaceColor, "colorOut");
		var _colorTolerance = shader_get_uniform(shd_ReplaceColor, "colorTolerance");
		var _colorBlend = shader_get_uniform(shd_ReplaceColor, "colorBlend");

		shader_set_uniform_f(_colorIn, 1.0, 0.0, 1.0, 1.0);
		shader_set_uniform_f(_colorOut, ghost_color_red, ghost_color_green, ghost_color_blue, 1.0);
		shader_set_uniform_f(_colorTolerance, 0.1, 0.1, 0.99, 1.0);
		shader_set_uniform_f(_colorBlend, 1.0);

		draw_self();
		shader_reset();
	}




	if (Game.debug && target_object != noone) {
		draw_circle_color(target_object.x, target_object.y, 3, c_red, c_red, false);
		draw_path(path, x, y, false);
	}
}