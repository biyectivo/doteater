/// @description 

if (!Game.paused && instantiated) {	
	if (!touched) {
		if (Game.pellet_active && state_name == "Flee") {		
			if (Game.option_value[? "Sounds"]) {
				audio_play_sound(snd_Bite, 1, false);
			}
			Game.num_ghosts_eaten_this_pellet++;
			Game.total_score = Game.total_score + 200*Game.num_ghosts_eaten_this_pellet;
			eaten = true;
			alarm[2] = 60;
		}
		else if (state_name == "Move") {
			with (obj_Player) {
				hp--;
				Game.num_lives--;
			}
			with (obj_Ghost) {
				instance_destroy();
			}
			if (Game.option_value[? "Sounds"]) {
				audio_play_sound(snd_Lose, 1, false);
			}
			if (Game.option_value[? "Music"]) {
				audio_pause_sound(Game.music_sound_id);
			}
		}
		touched = true;
	}
}