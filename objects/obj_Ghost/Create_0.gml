/// @description 
event_inherited();

name = "Ghost";
controllable = false;

// Beginning sprite frame index for each state
animation_startindices[? "Idle"] = 0;
animation_startindices[? "Move"] = 0;
animation_startindices[? "Flee"] = 12;
animation_startindices[? "Eaten"] = 8;
animation_startindices[? "Die"] = 0;

// Length in frames of animation for each state
animation_lengths[? "Idle"] = 2;
animation_lengths[? "Move"] = 2;
animation_lengths[? "Flee"] = 2;
animation_lengths[? "Eaten"] = 1;
animation_lengths[? "Die"] = 2;

// Animation speed for each frame - lower number means higher speed
animation_speeds[? "Idle"] = 8;
animation_speeds[? "Move"] = 8;
animation_speeds[? "Flee"] = 8;
animation_speeds[? "Eaten"] = 8;
animation_speeds[? "Die"] = 8;

// Spacing between frames of animation for each state (default = 1)
animation_spacings[? "Idle"] = 1;
animation_spacings[? "Move"] = 1;
animation_spacings[? "Flee"] = 1;
animation_spacings[? "Eaten"] = 1;
animation_spacings[? "Die"] = 1;

// Specific

path = path_add();
update_path = true;
ghost_speed = 1.6;

ghost_color_red = 0.0;
ghost_color_green = 1.0;
ghost_color_blue = 0.0;

path_type = PATH_TYPE.CHASE;

target_object = noone;

instantiated = true;
counter = 0;
eaten = false;

touched = false;