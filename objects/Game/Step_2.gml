//*****************************************************************************
// Handle camera
//*****************************************************************************


// Fullscreen has changed (either via ALT+TAB or via menu)
if (option_value[? "Fullscreen"] != window_get_fullscreen() || (keyboard_check(vk_alt) && keyboard_check(vk_enter))) {
	
	if (keyboard_check(vk_alt) && keyboard_check(vk_enter)) {
		option_value[? "Fullscreen"] = window_get_fullscreen();		
	}
	else {
		window_set_fullscreen(option_value[? "Fullscreen"]);
	}
	
	// Update graphics
	fnc_SetGraphics();
}



		
// Room-specific code
switch (room) {
	case room_Game_1:
		// Update game zoom
		/*
		if (mouse_wheel_down()) {
			camera_zoom = max(camera_min_zoom, camera_zoom - camera_zoom_speed);
		}
		else if (mouse_wheel_up()) {
			camera_zoom = min(camera_max_zoom, camera_zoom + camera_zoom_speed);	
		}*/
		
		// Update graphics
		//fnc_SetGraphics();
		
		// Update camera position		
		if (instance_exists(camera_target)) {
			
			// Get current camera position
			var _current_camera_x = camera_get_view_x(VIEW);
			var _current_camera_y = camera_get_view_y(VIEW);
	
			// Calculate offset for screen shake			
			var _offsetx = 0;
			var _offsety = 0;
			if (camera_shake) {
				var _offsetx = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
				var _offsety = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
			}
			
			// Get target camera position, centered around the target and lerped with camera smoothness
			var _destx = clamp(camera_target.x-adjusted_camera_width/2, 0, room_width-adjusted_camera_width);
			var _desty = clamp(camera_target.y-adjusted_camera_height/2, 0, room_height-adjusted_camera_height);
						
			var _target_camera_x = lerp(_current_camera_x, _destx, camera_smoothness) + _offsetx;
			var _target_camera_y = lerp(_current_camera_y, _desty, camera_smoothness) + _offsety;
			
			// Move camera
			camera_set_view_pos(VIEW, _target_camera_x, _target_camera_y);
		}

		break;
	default:	
		break;

}