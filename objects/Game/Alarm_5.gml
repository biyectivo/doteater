/// @description Restore level
if (Game.num_lives > 0) {
	fnc_CreateAgents();
	if (Game.option_value[? "Music"]) {
		audio_resume_sound(Game.music_sound_id);
	}
}
else {
	Game.lost = true;
	Game.paused = true;
	fnc_UpdateScoreboard();
}
	