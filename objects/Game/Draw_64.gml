//*****************************************************************************
// Draw GUI of Title, Options, Credits and Game
//*****************************************************************************

fnc_BackupDrawParams();


switch (room) {
	case room_Title:
		fnc_DrawMenu();
		break;
	case room_Options:
		fnc_DrawOptions();
		break;
	case room_Options_Controls:
		fnc_DrawOptionsControls();
		break;
	case room_Credits:
		fnc_DrawCredits();		
		break;
	case room_HowToPlay:
		fnc_DrawHowToPlay();
		break;
	default:		
		if (Game.paused) {
			if (Game.lost) {
				instance_deactivate_all(true);
				fnc_DrawYouLost();
			}
			else {				
				fnc_DrawPauseMenu();
			}
		}
		else {			
			fnc_DrawHUD();
		}
		break;
}

// Draw debug overlay on top of everything else
if (debug) {
	fnc_DrawDebug();
}

fnc_RestoreDrawParams();