//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
paused = false;
lost = false;
total_score = 0;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();


//*****************************************************************************
// Set camera target
//*****************************************************************************

camera_target = obj_Player;
camera_zoom = 1;
camera_min_zoom = 1;
camera_max_zoom = 1;
camera_zoom_speed = 0.5;
camera_shake = false;
camera_smoothness = 0.1;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "up";
control_indices[3] = "down";

controls[? "left"] = vk_left;
controls[? "right"] = vk_right;
controls[? "up"] = vk_up;
controls[? "down"] = vk_down;


control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "up"] = "Move Up";
control_names[? "down"] = "Move Down";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");


credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021\n\n[c_yellow]Programming: [c_white]biyectivo\n(Jose Alberto\nBonilla Vera)\n\n[c_yellow]Art: [c_white]KittenAnimates";


game_title = "Revolved";
scoreboard_game_id = "Revolved";
title_frame = 0;
alarm[3] = 10;

primary_gamepad = -1;

start_drag_drop = false;

current_step = 0;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter_fire = noone;

pause_screenshot = noone;

scribble_initialization = false;

//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;
http_get_id = -1;
http_return_status = noone;
scoreboard_queried = false;
scoreboard = [];

fnc_InitializeGameStartVariables();