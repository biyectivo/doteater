/// @description Screenshake
if (!Game.paused) {

	// Room-specific code
	switch (room) {
		case room_Game_1:
			Game.camera_shake = false;
			break;
		default:			
			break;
	}
}