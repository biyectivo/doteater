switch (room) {
	case room_Title:
		break;
	case room_Options:
	case room_Options_Controls:
	case room_HowToPlay:
	case room_Credits:
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Title;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
		break;
	default:
		if (!Game.lost) {			
			paused = !paused;		
			if (paused) {
				screen_save_part("_tmp_screenshot_1.png", 0, 0, window_get_width(), window_get_height());
				pause_screenshot = sprite_add("_tmp_screenshot_1.png", 0, false, false, 0, 0);
				instance_deactivate_all(true);
			}
			else {
				instance_activate_all();
			}
		}
		break;
}
