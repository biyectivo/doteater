// Scribble Font handling
//	Start initialisation:
//  The font directory is set as the root of the sandbox
//  The default font is set as "fnt_InGameTextue"
//  Automatic scanning for fonts ("autoscan") is turned off

/*
try {
	if (!scribble_initialization) {
		scribble_initialization = scribble_init("", "fnt_Menu", true);
	}
}
catch (ScribbleException) {
	show_debug_message(ScribbleException);
}
*/

scribble_font_add_all();

// Mouse cursor
window_set_cursor(cr_none);
cursor_sprite = spr_Cursor;

// Avoid automatic drawing of the application surface in order to draw it manually using the desired game resolution (independent of the window resolution)
application_surface_draw_enable(false);


// Go to next room
room_goto(room_Title);

//scr_JQBL_Tests();
