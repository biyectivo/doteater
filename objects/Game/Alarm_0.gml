/// @description Center screen
if (!window_get_fullscreen()) {
	window_center();
}