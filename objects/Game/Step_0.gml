if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (Game.lost && Game.paused && ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) {
	room_restart();
}


if (!Game.paused) {
	
	//*****************************************************************************
	// Hide collision layer
	//*****************************************************************************
	if (layer_exists(layer_get_id("lyr_Collision"))) {
		layer_set_visible(layer_get_id("lyr_Collision"), debug);
	}


	// Room-specific code
	switch (room) {		
		case room_Game_1:
			if (instance_exists(obj_Player)) {
				current_step++;
			}
			show_debug_message(string(instance_number(obj_Dot)));
			if (instance_number(obj_Dot) == 1) {
				fnc_CreatePellets();
				num_lives++;
			}
			break;
		default:	
			break;
	}
}