
//*****************************************************************************
// Set things to do at Room Start, per each room - such as room dimensions
//*****************************************************************************
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	

// Enable views
view_enabled = true;
view_visible[0] = true;

// Update graphics (camera, window, app surface, GUI layer)

fnc_SetGraphics();
switch (room) {	
	case room_Game_1:
		
		// Define pathfinding grid
		
		grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
		// Add collision tiles to grid			
		for (var _i = 0; _i < room_width/GRID_RESOLUTION; _i++) {
			for (var _j = 0; _j < room_height/GRID_RESOLUTION; _j++) {				
				if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Collision")), _i*GRID_RESOLUTION, _j*GRID_RESOLUTION) != 0) {
					mp_grid_add_cell(grid, _i, _j);
				}
			}
		}
		
		// Initialize particle system and emitters
		
		particle_system = part_system_create_layer("lyr_Particles", true);
		particle_emitter_fire = part_emitter_create(particle_system);
		
		// Instantiate player
		if (instance_exists(obj_Player)) {
			with (obj_Player) {
				event_perform(ev_other, ev_user0);
			}	
		}	
		else {
			show_debug_message("Error, no player");
		}
		
		with (obj_Ghost) {
			event_perform(ev_other, ev_user0);
		}
		
		/********* Game specific code **********/
		
		
		fnc_InitializeGameStartVariables();
		
		fnc_CreateAgents();
			
		fnc_CreatePellets();
		
		
		if (option_value[? "Music"]) {
			music_sound_id = audio_play_sound(snd_Music, 1, true);
		}
		
		break;
	default:
		break;
}
