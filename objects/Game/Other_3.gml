mp_grid_destroy(grid);

if (ds_exists(option_type, ds_type_map)) {
	ds_map_destroy(option_type);
}
if (ds_exists(option_value, ds_type_map)) {
	ds_map_destroy(option_value);
}

if (ds_exists(controls, ds_type_map)) {
	ds_map_destroy(controls);
}

if (ds_exists(control_names, ds_type_map)) {
	ds_map_destroy(control_names);
}

if (part_emitter_exists(particle_system, particle_emitter_fire)) {
	part_emitter_destroy(particle_system, particle_emitter_fire);
}
if (part_system_exists(particle_system)) {
	part_system_destroy(particle_system);	
}
