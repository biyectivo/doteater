var r_str = "null";

show_debug_message("Async load");
show_debug_message(async_load[? "id"]);
show_debug_message(async_load[? "status"]);
show_debug_message(async_load[? "result"]);
show_debug_message(async_load[? "url"]);
show_debug_message(async_load[? "http_status"]);

http_return_status = async_load[? "http_status"];
if (async_load[? "status"] == 0 && http_call == "query_scoreboard") {
	scoreboard = json_parse(async_load[? "result"]);
}