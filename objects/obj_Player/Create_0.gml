/// @description 
event_inherited();


// Beginning sprite frame index for each state
animation_startindices[? "Idle"] = 0;
animation_startindices[? "Move"] = 4;
animation_startindices[? "Die"] = 12;

// Length in frames of animation for each state
animation_lengths[? "Idle"] = 1;
animation_lengths[? "Move"] = 2;
animation_lengths[? "Die"] = 9;

// Animation speed for each frame - lower number means higher speed
animation_speeds[? "Idle"] = 8;
animation_speeds[? "Move"] = 8;
animation_speeds[? "Die"] = 8;

// Variable to hold the direction the player wants to go to whenever possible
current_horizontal_direction = 0;
current_vertical_direction = 0;

queued_horizontal_direction = 0;
queued_vertical_direction = 0;

just_teleported_id = noone;
