/// @description 
var _other_object_id;
with (obj_Teleport) {
	if (teleport_id == other.target_id) {
		_other_object_id = id;
	}
}

if (obj_Player.object_index == other.object_index) {
	obj_Player.just_teleported_id = teleport_id;
	obj_Player.alarm[2] = 60*4;
}


switch (teleport_direction) {
	case "right":
		other.x = _other_object_id.bbox_right + TILE_SIZE/2 - 1;
		other.y = _other_object_id.y;
		break;
	case "left":
		other.x = _other_object_id.bbox_left - TILE_SIZE/2;
		other.y = _other_object_id.y;
		break;
	case "up":
		other.y = _other_object_id.bbox_top - TILE_SIZE/2;
		other.x = _other_object_id.x;
		break;
	case "down":
		other.y = _other_object_id.bbox_bottom + TILE_SIZE/2 - 1;
		other.x = _other_object_id.x;
		break;
	default:
		break;
}

