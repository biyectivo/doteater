/// @description 
if (!Game.paused && instantiated) {	
	if (alarm[0] == -1) {
		Game.total_score = Game.total_score + 10;
		alarm[0] = 60;
		
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Dot, 1, false);
		}
	}
}