
/// @function fnc_SetGraphics
/// @description Update graphics

function fnc_SetGraphics() {

	if (argument_count>1) {
		var _RESOLUTION_W = argument[0];
		var _RESOLUTION_H = argument[1];
	}
	else {
		var _RESOLUTION_W = BASE_RESOLUTION_W;
		var _RESOLUTION_H = BASE_RESOLUTION_H;
	}
	

	if (SELECTED_SCALING == SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION) { // Adjust desired Game resolution (camera) to match display size/aspect ratio and then set window equal to game resolution		
		if (DISPLAY_WIDTH < DISPLAY_HEIGHT) { // Portrait aspect ratio
			adjusted_camera_width = min(_RESOLUTION_W, DISPLAY_WIDTH);
			adjusted_camera_height = floor(adjusted_camera_width / ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_camera_height = min(_RESOLUTION_H, DISPLAY_HEIGHT);
			adjusted_camera_width = floor(adjusted_camera_height * ASPECT_RATIO);
		}
		
		if (window_get_fullscreen()) {
			adjusted_window_width = DISPLAY_WIDTH;
			adjusted_window_height = DISPLAY_HEIGHT;
		}
		else {			
			adjusted_window_width = adjusted_camera_width;
			adjusted_window_height = adjusted_camera_height;
		}
	}
	else if (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION) { // Leave Game resolution (camera) fixed (as long as <= window) and adjust desired window resolution to match display size/aspect ratio
		
		if (DISPLAY_WIDTH < DISPLAY_HEIGHT) { // Portrait aspect ratio
			adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
			adjusted_window_height = floor(adjusted_window_width / ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
			adjusted_window_width = floor(adjusted_window_height * ASPECT_RATIO);
		}
		
		adjusted_camera_width = min(_RESOLUTION_W, BASE_WINDOW_SIZE_W);
		adjusted_camera_height = min(_RESOLUTION_H, BASE_WINDOW_SIZE_H);		
		
	}
	else { // Resolution scaled to window size
		
		if (DISPLAY_WIDTH < DISPLAY_HEIGHT) { // Portrait aspect ratio
			adjusted_camera_width = min(_RESOLUTION_W, DISPLAY_WIDTH);
			adjusted_camera_height = floor(adjusted_camera_width / ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_camera_height = min(_RESOLUTION_H, DISPLAY_HEIGHT);
			adjusted_camera_width = floor(adjusted_camera_height * ASPECT_RATIO);
		}
				
		if (DISPLAY_WIDTH < DISPLAY_HEIGHT) { // Portrait aspect ratio
			adjusted_window_width = min(BASE_WINDOW_SIZE_W, DISPLAY_WIDTH);
			adjusted_window_height = floor(adjusted_window_width / ASPECT_RATIO);
		}
		else { // Landscape aspect ratio
			adjusted_window_height = min(BASE_WINDOW_SIZE_H, DISPLAY_HEIGHT);
			adjusted_window_width = floor(adjusted_window_height * ASPECT_RATIO);
		}
		
	}
	
	// Redefine camera size
	camera_set_view_size(VIEW, adjusted_camera_width, adjusted_camera_height);
		
	// Set window size
	window_set_size(adjusted_window_width, adjusted_window_height);
	
	// Resize the application surface to the desired game resolution width/height
	surface_resize(application_surface, adjusted_camera_width, adjusted_camera_height);
	
	// Center the window (on the next few step)
	if (CENTER_SCREEN) {
		alarm[0] = 2;
	}
	
	if (Game.debug) {
		show_debug_message("------------------------------");
		show_debug_message("* Graphics controller script *");
		show_debug_message("------------------------------");
		show_debug_message(" Room: "+string(room_get_name(room)));
		show_debug_message(" Event: "+string(event_type));
		show_debug_message(" Scaling Mode: "+string(SELECTED_SCALING));
		show_debug_message(" Fullscreen: "+string(window_get_fullscreen()));
		show_debug_message(" Resolution (camera):");
		show_debug_message("  Desired W = "+string(_RESOLUTION_W)+", adjusted W = "+string(adjusted_camera_width));
		show_debug_message("  Desired H = "+string(_RESOLUTION_H)+", adjusted H = "+string(adjusted_camera_height));
		show_debug_message(" Window:");
		show_debug_message("  Desired W = "+string(BASE_WINDOW_SIZE_W)+", adjusted W = "+string(adjusted_window_width));
		show_debug_message("  Desired H = "+string(BASE_WINDOW_SIZE_H)+", adjusted H = "+string(adjusted_window_height));
		show_debug_message(" Display:");
		show_debug_message("  "+string(DISPLAY_WIDTH)+"x"+string(DISPLAY_HEIGHT));
		show_debug_message(" Application surface:");
		show_debug_message("  "+string(adjusted_camera_width)+"x"+string(adjusted_camera_height));
		show_debug_message(" GUI Layer:");
		show_debug_message("  "+string(adjusted_window_width)+"x"+string(adjusted_window_height));
	}
}



/*------------------------------------------------------------------*/



function fnc_UpdateLevel() {
	
}


function fnc_InitializeAlarms() {
	/*alarm[0] = 60*1;*/
}

function fnc_InitializeGameStartVariables() {
	paused = false;
	lost = false;
	level = 1;
	total_score = 0;
	pellet_active = false;
	num_ghosts_eaten_this_pellet = 0;
	num_ghosts = 4;
	num_lives = 3;
	death_message_chosen = "";
	current_step = 0;	
	scoreboard_queried = false;
	scoreboard = [];
	http_call = "";
}

function fnc_UpdateScoreboard() {
	if (ENABLE_SCOREBOARD && Game.lost && Game.total_score > 0) {		
		http_call = "update_scoreboard";
		var _scoreboard_url = "http://www.biyectivo.com/misc/scoreboard/score_insert.php";
		var _hash = sha512_calc(Game.option_value[? option_items[5]]+Game.scoreboard_game_id+string(Game.total_score)+SCOREBOARD_SALT);
		var _params = "user="+Game.option_value[? option_items[5]]+"&game="+Game.scoreboard_game_id+"&score="+string(Game.total_score)+"&h="+_hash;
				
		http_get_id = http_get(_scoreboard_url + "?" + _params);
	}	
}

function fnc_AgentAlignedWithTile(_obj) {
	return ((_obj.bbox_bottom+1) % TILE_SIZE == 0) && ((_obj.bbox_right+1) % TILE_SIZE == 0);
}


function fnc_CreateAgents() {
	// Create player
		
	var _id = instance_create_layer(19*TILE_SIZE+TILE_SIZE/2, 19*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Player);
	with (obj_Player) {
		event_perform(ev_other, ev_user0);
	}
		
	// Create ghosts
		
	var _red = [0.0, 1.0, 1.0, 0.0];
	var _green = [1.0, 0.0, 1.0, 1.0];
	var _blue = [0.0, 0.0, 0.0, 1.0];
	var _inix = [18, 18, 20, 20];
	var _iniy = [14, 15, 14, 15];
	var _paths = [PATH_TYPE.CHASE, PATH_TYPE.CHASE, PATH_TYPE.GET_AHEAD, PATH_TYPE.DUMB];
	for (var _i=0; _i<num_ghosts; _i++) {
		if (_i<4) {
			var _id = instance_create_layer(_inix[_i]*TILE_SIZE+TILE_SIZE/2, _iniy[_i]*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Ghost);
			with (_id) {			
				ghost_color_red = _red[_i];
				ghost_color_green = _green[_i];
				ghost_color_blue = _blue[_i];
				path_type = _paths[_i];
				event_perform(ev_other, ev_user0);	
			}
		}
		else {
			var _id = instance_create_layer(choose(18,20)*TILE_SIZE+TILE_SIZE/2, choose(14,15)*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Ghost);
			with (_id) {
				ghost_color_red = choose(0,1);
				ghost_color_green = choose(0,1);
				ghost_color_blue = choose(0,1);
				path_type = choose(PATH_TYPE.CHASE, PATH_TYPE.GET_AHEAD);
				event_perform(ev_other, ev_user0);
			}
		}
	}	
}

function fnc_CreatePellets() {
	// Create pellets
	var _id = instance_create_layer(7*TILE_SIZE+TILE_SIZE/2, 4*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Pellet);
	var _id = instance_create_layer(31*TILE_SIZE+TILE_SIZE/2, 4*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Pellet);
	var _id = instance_create_layer(7*TILE_SIZE+TILE_SIZE/2, 26*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Pellet);
	var _id = instance_create_layer(31*TILE_SIZE+TILE_SIZE/2, 26*TILE_SIZE+TILE_SIZE/2, layer_get_id("lyr_Agents"), obj_Pellet);
		
	// Fill dots
	var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tilemap"));
	var _collision_tilemap = layer_tilemap_get_id(layer_get_id("lyr_Collision"));
	for (var _col=0; _col<tilemap_get_width(_tilemap); _col++) {
		for (var _row=0; _row<tilemap_get_height(_tilemap); _row++) {
			//show_debug_message(string(_col)+","+string(_row)+": "+string(tilemap_get(_tilemap, _col, _row)));
			if (tilemap_get(_collision_tilemap, _col, _row) == 0 && tilemap_get(_tilemap, _col, _row) == 8) {
				var _x = TILE_SIZE * _col + TILE_SIZE/2;
				var _y = TILE_SIZE * _row + TILE_SIZE/2;
				if (!position_meeting(_x, _y, obj_Pellet) && !position_meeting(_x, _y, obj_Player)) {
					instance_create_layer(_x, _y, layer_get_id("lyr_Instances"), obj_Dot);					
				}
			}
		}
	}	
}