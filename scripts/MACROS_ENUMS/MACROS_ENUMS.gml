// =============================================== Graphics =============================================== 
#macro VIEW view_camera[0]

// NOTE: Room size, does not play here. it can be as big or as little.

// 0 - Display size and aspect ratio
// Leave it calculated to get it automatically, or you can force it to a specific size to simulate other screens.
#macro DISPLAY_WIDTH display_get_width()
#macro DISPLAY_HEIGHT display_get_height()
//#macro ASPECT_RATIO DISPLAY_WIDTH/DISPLAY_HEIGHT
#macro ASPECT_RATIO 4/3

enum SCALING_TYPE {
	WINDOW_SAME_AS_RESOLUTION, // set resolution and let window adapt to that resolution
	WINDOW_INDEPENDENT_OF_RESOLUTION, // Set resolution AND set window size independently (e.g. bosom)
	RESOLUTION_SCALED_TO_WINDOW // set resolution AND window size and let resolution scale to window size
}

#macro SELECTED_SCALING SCALING_TYPE.RESOLUTION_SCALED_TO_WINDOW

// 1 - Base resolution: this is the game resolution it's designed for (this will be the camera view size)
//		Notes: to achieve a perfect scaling for a 16:9 aspect ratio, one can take these values:
//		1x	1920x1080
//		2x	960x540
//		3x	640x360
//		4x	480x270
//		5x	384x216
//		6x	320x180
#macro BASE_RESOLUTION_W 640
#macro BASE_RESOLUTION_H 480

// 2 - Window resolution: OPTIONAL. Set this only if you want the WINDOW to be different size than the actual GAME RESOLUTION
// Applies to WINDOW_INDEPENDENT_OF_RESOLUTION (in this case there will be no scaling) and to RESOLUTION_SCALED_TO_WINDOW (in this case the resolution will scale)
// For RESOLUTION_SCALED_TO_WINDOW, for landscape aspect ratios, it will adjust the width automatically (so choose height wisely); for portrait aspect ratios, it will adjust the height automatically (so choose width wisely)
#macro BASE_WINDOW_SIZE_W BASE_RESOLUTION_W
#macro BASE_WINDOW_SIZE_H BASE_RESOLUTION_H

#macro CENTER_SCREEN true

// =============================================== Technical =============================================== 

#macro TILE_SIZE 16
#macro GRID_RESOLUTION 16
#macro GAMEPAD_THRESHOLD 0.3

#macro ENABLE_SCOREBOARD true
#macro SCOREBOARD_SALT "ScOrEsPaCe11"

enum TRANSITION {
	FADE_OUT
}

enum FACING {
	EAST,
	WEST,
	NORTH,
	SOUTH
}


#macro SCREENSHAKE_STRENGTH 10
#macro SCREENSHAKE_DURATION 30

// =============================================== Game-Specific =============================================== 

enum PATH_TYPE {
	CHASE, 
	GET_AHEAD,
	DUMB
}