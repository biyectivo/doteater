
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = window_get_width();
		var _h = window_get_height();
		
		var _y_title = -50;
		
		//scribble("[fnt_Title][fa_middle][fa_center][c_gray][scale,1.2]"+game_title).draw(_w/2+3, _y_title+3);
		//scribble_draw(_w/2+3, _y_title+3, "[fnt_Title][fa_middle][fa_center][c_gray][scale,1.2]"+game_title);
		//scribble("[fnt_Title][fa_middle][fa_center][c_gray][scale,1.2]"+game_title).draw(_w/2+2, _y_title+2);
		//scribble("[fnt_Title][fa_middle][fa_center][c_gray][scale,1.2]"+game_title).draw(_w/2+1, _y_title+1);
		//scribble("[fnt_Title][fa_middle][fa_center][scale,1.2]"+_title_color+game_title).draw(_w/2, _y_title);
		draw_sprite_ext(spr_Title, title_frame, (window_get_width()-2.5*sprite_get_width(spr_Title))/2, _y_title, 2.5, 2.5, 0, c_white, 1);
	
		var _n = array_length(menu_items);
		var _startY = _y_title + 300;
		var _spacing = 40;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center]"+_link_color+menu_items[_i], "[fa_middle][fa_center]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
		
		
		
		//scribble_draw(_w/2, _h-20, "[fa_center][fa_middle][fnt_InGameText]A game made by José Bonilla");
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);
		with  (transition_id) {
			destination_room = room_HowToPlay;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
	}


	function fnc_Menu_2 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Options;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
	}

	
	function fnc_Menu_3 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Credits;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = window_get_width();
		var _h = window_get_height();

		var _title_color = "[$fffdff]";
		var _main_text_color = "[$fffdff]";
		var _link_color = "[$fffdff]";
		var _link_hover_color = "[$0BFFE6]";
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		scribble("[fa_middle][fa_center][fnt_Title]"+_title_color+"Options").draw(_w/2, _y_title);
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);	
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp = scribble("[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]);
				
				var _slider_start = _w/2-100 + _temp.get_width() + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					scribble("[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%").draw(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30);
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);			
		instance_activate_object(cls_Transition);
		with  (transition_id) {
			destination_room = room_Options_Controls;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
		Game.paused = false;
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = window_get_width();
		var _h = window_get_height();
		
		var _title_color = "[$fffdff]";
		var _main_text_color = "[$fffdff]";
		var _link_color = "[$fffdff]";
		var _link_hover_color = "[$0BFFE6]";
		
		var _y_title = 60;
		scribble("[fa_middle][fa_center][fnt_Title]Controls").draw(_w/2, _y_title);
		
		/*if (wait_for_input) {
			scribble_draw(_w/2, _h-30, "[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
			fnc_Link(_w/2-150, _y_title+60, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+"Fly Left: ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+"Fly Left: ", noone, 0);
			fnc_AssignControls("left");
		}
		else {
			fnc_Link(_w/2-150, _y_title+60, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+"Fly Left: "+fnc_KeyToString(controls[? "left"]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+"Fly Left: "+fnc_KeyToString(controls[? "left"]), fnc_AssignControls, "left");
		}*/
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				scribble("[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP").draw(_w/2, _h-30);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Controllers"), cls_Transition);			
		instance_activate_object(cls_Transition);
		with  (transition_id) {
			destination_room = room_Title;
			transition_type = TRANSITION.FADE_OUT;
			max_time = 30;
			event_perform(ev_other, ev_user0);
		}
		Game.paused = false;
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _y_title = 60;
	scribble("[fnt_Title][fa_middle][fa_center]Credits").draw(_w/2, _y_title);
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		var _text = scribble(credits[_i]).draw(_w/2, _startY + _i*_spacing);
	}	
	
	fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]Return to Main Menu", "[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = window_get_width();
	var _h = window_get_height();
	
	var _y_title = 60;
	scribble("[fnt_Title][fa_middle][fa_center]Help").draw(_w/2, _y_title);
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	scribble("[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]").draw(_w/2, _startY);
	scribble("[fnt_MiniText][fa_center][fa_middle]Move to collect [spr_Dot] and avoid ghosts!").draw(_w/2, _startY+40);
	scribble("[fnt_MiniText][fa_center][fa_middle](Remap controls in options)").draw(_w/2, _startY+40*2);
	scribble("[fnt_MiniText][fa_center][fa_middle]Grab [spr_Pellet] to eat ghosts for a short time!").draw(_w/2, _startY+40*3);
	scribble("[fnt_MiniText][fa_center][fa_middle]Watch out for side effects though!").draw(_w/2, _startY+40*4);	
	scribble("[fnt_MiniText][fa_center][fa_middle]Beat the world record!").draw(_w/2, _startY+40*6);
	
	
	fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]Return to Main Menu", "[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = window_get_width();
	var _h = window_get_height();
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	scribble("[fa_center][fnt_Menu]Game Paused\nPress ESC to resume").draw(_w/2, 30);
	
	
	fnc_Link(_w/2, _h-60, "[fa_center][fa_middle]Return to Main Menu", "[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	var _w = window_get_width();
	var _h = window_get_height();
	draw_set_alpha(0.7);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	
	audio_stop_all();
	scribble("[fa_center][fnt_Menu][scale,2]+[/s]").draw(_w/2, 40);
	if (death_message_chosen == "") {
		death_message_chosen = choose("Requiescat in Pace", "You died", "Goodbye cruel world", "You bit the dust", "You met your maker", "You passed away", "Boom.");
	}
	scribble("[fa_center][fnt_Menu]"+death_message_chosen).draw(_w/2, 80);
	scribble("[fa_center][fnt_Menu][rainbow]Score: "+string(Game.total_score)+"[/rainbow]").draw(_w/2, 110);
	
	if (ENABLE_SCOREBOARD) {
		// Scoreboard
		if (!scoreboard_queried) {
			http_call = "query_scoreboard";
			var _scoreboard_url = "http://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
			http_get_id = http_get(_scoreboard_url);
			scoreboard_queried = true;		
		}
		else {
			if (http_return_status == 200) {
				var _n = array_length(scoreboard);
			
				for (var _i=0; _i<_n; _i++) {
					scribble("[fa_middle][fa_right][fnt_MiniText][c_white]#"+string(_i+1)).draw(offset_x_GUI+200, 180+_i*30);
					scribble("[fa_middle][fa_left][fnt_MiniText][c_white]"+scoreboard[_i].username).draw(offset_x_GUI+220, 180+_i*30);
					scribble("[fa_middle][fa_right][fnt_MiniText][c_white]"+string(scoreboard[_i].game_score)).draw(offset_x_GUI+500, 180+_i*30);
				}
			}		
			else if (http_return_status != noone) {
				scribble("[fa_middle][fa_center]No/bad connection \n Cannot show scoreboard").draw(_w/2, 140);
				scoreboard_queried = false;	
			}
			else {
				scribble("[fa_middle][fa_center]Loading scoreboard...").draw(_w/2, 140);
			}
		}
	}
	
	fnc_Link(_w/2, _h-120, "[fa_center][fa_middle]ENTER to restart", "[fa_center][fa_middle][$0BFFE6]ENTER to restart", fnc_TryAgain, 0);
	fnc_Link(_w/2, _h-60, "[fa_center][fa_middle]Return to Main Menu", "[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawHUD() {
	fnc_BackupDrawParams();
	// Draw the HUD
	
	var _score_string = "";
	var _score = string_copy("00000000"+string(Game.total_score), string_length("00000000"+string(Game.total_score))-7, 8);
	for (var _i=1; _i<=string_length(_score); _i++) {		
		_score_string = _score_string + "[spr_Digits,"+string_copy(_score, _i, 1)+"]";
	}
		
	var _lives_string = "";
	for (var _i=0; _i<num_lives; _i++) {
		_lives_string = _lives_string + "[spr_Player,0]";
	}
	
	scribble("[fnt_Menu][fa_center][fa_middle][c_yellow][scale,0.8]SCORE: [scale,1]"+_score_string+"     "+_lives_string).draw(window_get_width()/2, 20);
	
	fnc_RestoreDrawParams();
}

#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.7);
		//draw_rectangle(0, 0, adjusted_window_width, adjusted_window_height, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(10, 10,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(10, 25, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.state_name);
		}
		else {
			draw_text(10, 25, "Player not in room");	
		}
		var _spacing = 30;
		
		var _debug_data = [];
		
		_debug_data[0] = "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")";
		_debug_data[1] = "Requested scaling type: "+string(SELECTED_SCALING)+" ("+(SELECTED_SCALING==SCALING_TYPE.WINDOW_SAME_AS_RESOLUTION ? "Window determined by resolution" : (SELECTED_SCALING == SCALING_TYPE.WINDOW_INDEPENDENT_OF_RESOLUTION ? "Window independent of resolution" : "Resolution scaled to window"));
		_debug_data[2] = "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H);
		_debug_data[3] = "Actual base resolution: "+string(adjusted_camera_width)+"x"+string(adjusted_camera_height);
		_debug_data[4] = "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H);
		_debug_data[5] = "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height);
		_debug_data[6] = "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")";
		_debug_data[7] = "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")";
		_debug_data[8] = "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")";
		_debug_data[9] = "GUI Layer: "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")";
		_debug_data[10] = "Mouse: "+string(mouse_x)+","+string(mouse_y);
		_debug_data[11] = "Device mouse 0: "+string(device_mouse_x(0))+","+string(device_mouse_y(0));
		_debug_data[12] = "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0));		
		_debug_data[13] = "Paused: "+string(paused);
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(10, 40+_i*15, _debug_data[_i]);
		}
	}
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _scribble_normal = scribble(_text);
	var _scribble_mouseover = scribble(_text_mouseover);
		
	var _bbox = scribble(_text).get_bbox(_x, _y);
	// Fix for scribble bug confirmed by @JujuAdams
	/*
	_bbox[1] = _y - scribble_get_height(_scribble_normal)/2;
	_bbox[3] = _y + scribble_get_height(_scribble_normal)/2;
	*/
	// ---
	
	var _mousex = device_mouse_x_to_gui(0);
	var _mousey = device_mouse_y_to_gui(0);

	var _mouseover = _mousex >= _bbox.left && _mousey >= _bbox.top && _mousex <= _bbox.right && _mousey <= _bbox.bottom;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		draw_rectangle(_bbox.left, _bbox.top, _bbox.right, _bbox.bottom, false);
	}
	
	if (_click && _mouseover) {
		_scribble_mouseover.draw(_x, _y);
		if (_callback != noone) {
			script_execute(_callback, _param);
		}
	}
	else if (_mouseover) {
		_scribble_mouseover.draw(_x, _y);
	}
	else {
		_scribble_normal.draw(_x, _y);
	}

}

function fnc_ExecuteMenu(_param) {
	script_execute(asset_get_index("fnc_Menu_"+string(_param)));
}

function fnc_ExecuteOptions(_param) {
	script_execute(asset_get_index("fnc_Options_"+string(_param)));
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion