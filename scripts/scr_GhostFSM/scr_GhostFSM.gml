
/// @function fnc_GhostFSM_Idle
/// @description Idle state for player
function fnc_GhostFSM_Idle() {

	eaten = false;
	touched = false;
	if (random(1) < 0.01) {
		// Transition to other states
		fnc_GhostFSM_Transition();
	}
}


/// @function fnc_GhostFSM_Move
/// @description Move state for player
function fnc_GhostFSM_Move() {
	counter++;
	
	if (obj_Player.just_teleported_id != noone) {		
		with (obj_Teleport) {
			if (teleport_id == obj_Player.just_teleported_id) {
				if (distance_to_object(other) < TILE_SIZE*6) {
					other.target_object = id;
				}
				else {
					other.target_object = obj_Player;
				}
			}
		}		
	}
	else {
		target_object = obj_Player;
	}
	
	if (target_object == obj_Player) {
		if (path_type == PATH_TYPE.CHASE) {		
			var _x = obj_Player.x;
			var _y = obj_Player.y;
		}
		else if (path_type == PATH_TYPE.GET_AHEAD) {
			var _x = obj_Player.x + 2*TILE_SIZE;
			var _y = obj_Player.y;
		}
		else {
			var _x = obj_Player.x + irandom_range(-3, 3)*TILE_SIZE;
			var _y = obj_Player.y + irandom_range(-3, 3)*TILE_SIZE;
		}
	}
	else {
		var _x = target_object.x;
		var _y = target_object.y;
	}
	
	if (update_path && mp_grid_path(Game.grid, path, x, y, _x, _y, false)) {		
		path_start(path, ghost_speed, path_action_stop, false);
		update_path = false;
	}
	
	if (counter % (TILE_SIZE/ghost_speed) == 0) {
		update_path = true;
	}
	
	switch (direction) {
		case 0:
			facing = FACING.EAST;
			break;
		case 90:
			facing = FACING.NORTH;
			break;
		case 180:
			facing = FACING.WEST;
			break;
		case 270:
			facing = FACING.SOUTH;
			break;
		default:
			break;
	}

	// Transition to other states
	fnc_GhostFSM_Transition();
	
}


/// @function fnc_GhostFSM_Flee
/// @description Flee state 
function fnc_GhostFSM_Flee() {
	counter++;
		
	
	// Flee	
	
	if (update_path) {
		//show_debug_message("time to update "+string(id));
		do {
			var _x = irandom_range(0, room_width) div TILE_SIZE;
			var _y = irandom_range(0, room_height) div TILE_SIZE;
		}
		until (tilemap_get(layer_tilemap_get_id(layer_get_id("lyr_Tilemap")), _x, _y) == 8);
		
		_x = _x*TILE_SIZE + TILE_SIZE/2;
		_y = _y*TILE_SIZE + TILE_SIZE/2;
		
		update_path = false;
		if (mp_grid_path(Game.grid, path, x, y, _x, _y, false)) {		
			path_start(path, ghost_speed/2, path_action_stop, false);
		}
		//show_debug_message("Ghost "+string(id)+" heading to "+string(_x)+","+string(_y));
	}	
	
	if (path_position == 1) {
		update_path = true;
	}
	
	switch (direction) {
		case 0:
			facing = FACING.EAST;
			break;
		case 90:
			facing = FACING.NORTH;
			break;
		case 180:
			facing = FACING.WEST;
			break;
		case 270:
			facing = FACING.SOUTH;
			break;
		default:
			break;
	}

	// Transition to other states
	fnc_GhostFSM_Transition();
}




/// @function fnc_GhostFSM_Eaten
/// @description Eaten state 
function fnc_GhostFSM_Eaten() {
	counter++;
		
	
	// Eaten	
	
	if (update_path) {
		//show_debug_message("time to update "+string(id));
		var _x = choose(18, 20);
		var _y = choose(14, 15);
		
		_x = _x*TILE_SIZE + TILE_SIZE/2;
		_y = _y*TILE_SIZE + TILE_SIZE/2;
		
		update_path = false;
		if (mp_grid_path(Game.grid, path, x, y, _x, _y, false)) {		
			path_start(path, ghost_speed, path_action_stop, false);
		}		
	}	
	
	switch (direction) {
		case 0:
			facing = FACING.EAST;
			break;
		case 90:
			facing = FACING.NORTH;
			break;
		case 180:
			facing = FACING.WEST;
			break;
		case 270:
			facing = FACING.SOUTH;
			break;
		default:
			break;
	}

	// Transition to other states
	fnc_GhostFSM_Transition();
}


/// @function fnc_GhostFSM_Attack
/// @description Attack state for player
function fnc_GhostFSM_Attack() {
	
}

function fnc_GhostFSM_Die() {
	
}

function fnc_GhostFSM_Animate(_reset_animation) {
	if (state_name != "Die") {
		animation_frame_count = _reset_animation ? 0 : ((animation_frame_count + 1) % animation_lengths[? animation_name]);
		animation_frame = animation_frame_count * animation_spacings[? animation_name];
		
		/* Currently working in 4-directions if the sprite is a multi-frame sprite (imported from spritesheet) like so:
		
			[ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ][ ]
			 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23
		    |----------||----------------------||----------------------------------|
			    idle             move                          attack
		     ^           ^                       ^
			startindex   startindex              startindex
			
			order of directions determined by FACING enum
			sprite should have centerpoint as origin
		
		*/
		
		if (animation_dirs >= 4) {
			image_index = animation_startindices[? animation_name] + facing * animation_lengths[? animation_name] + animation_frame;
			if  (Game.debug) {
				show_debug_message(name+" @ "+state_name+" facing "+string(facing)+": ("+string(_reset_animation)+") "+string(animation_startindices[? animation_name])+"+"+string(facing)+"*"+string(animation_lengths[? animation_name])+"+"+string(animation_frame)+"="+string(image_index));
			}
		}
		else if (animation_dirs	== 2) {
			image_index = animation_startindices[? animation_name] + animation_frame;			
			image_xscale = (facing == FACING.EAST ? -base_xscale : base_xscale);			
			image_index = player_horizontal_input == -1 ? 1 : (player_horizontal_input == 1 ? 2 : 0);
			
		}
		else {
			image_index = animation_startindices[? animation_name] + animation_frame;
		}
	}
	else {
		animation_frame = _reset_animation ? 0 : min(animation_frame + animation_spacings[? animation_name], animation_lengths[? animation_name]-1);
		image_index = animation_startindices[? animation_name] + animation_frame;		
		image_xscale = base_xscale;		
	}	
}

function fnc_GhostFSM_ResetAnimation() {
	fnc_GhostFSM_Animate(true);
	alarm[0] = animation_speeds[? animation_name];
}

function fnc_GhostFSM_Transition() {
	last_state = state;
	last_state_name = state_name;
	
	// Transition matrix
	switch (state_name) {
		case "Idle":
			state = fnc_GhostFSM_Move;
			state_name = "Move";
			break;
		case "Move":
			if (Game.pellet_active) {
				state = fnc_GhostFSM_Flee;
				state_name = "Flee";		
			}
			break;
		case "Flee":
			if (!Game.pellet_active) {
				state = fnc_GhostFSM_Move;
				state_name = "Move";		
			}
			else if (eaten) {
				state = fnc_GhostFSM_Eaten;
				state_name = "Eaten";
			}
			break;
		case "Eaten":
			if (path_position == 1) {
				state = fnc_GhostFSM_Idle;
				state_name = "Idle";
			}
			break;
		default:
			break;
	}
	
	
	// Reset animation
	reset_animation = (state != last_state);
	animation_name = state_name;
	
	if (reset_animation) {
		counter = 0;
		update_path = true;		
		show_debug_message(name+" transitioning from "+string(last_state)+" "+last_state_name+" to "+string(state)+" "+state_name);
		fnc_GhostFSM_ResetAnimation();
	}
}