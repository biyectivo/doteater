enum JBQL_TYPES {
	CHAR,
	NUM,
	DATE,
	TIME,
	DATETIME,
	BOOL	
}




/// @function		fnc_JBQL_CreateTableCSV()
/// @description	Load a CSV into a JBQL table. The CSV should be comma-separated. If it has column names in the first row, these will be taken for the column names in JBQL.
/// @param			tablename - Name of the JBQL table to create
/// @param			filename - Name of the CSV file to load.
/// @param			column - Optional boolean parameters to specify whether the CSV has column names on the first row. Default: true.
/// @return			The JBQL table (struct)
function fnc_JBQL_CreateTableCSV() {
	if (argument_count == 0) {		
		throw ("[fnc_JBQL_CreateTableCSV] Filename argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _table_name = argument[0];
	}
	if (argument_count >= 2) {		
		var _filename = argument[1];
		if (!file_exists(_filename)) {
			show_debug_message(working_directory);
			show_debug_message(_filename);
			throw ("[fnc_JBQL_CreateTableCSV] Filename provided does not exist.");
		}
	}
	if (argument_count >= 3) {
		var _has_column_names = argument[2];
	}
	else {
		var _has_column_names = true;
	}
	
	// Load data and delete first row
	var _grid = load_csv(_filename);
	ds_grid_set_grid_region(_grid, _grid, 0, 1, ds_grid_width(_grid)-1, ds_grid_height(_grid)-1, 0, 0);
	ds_grid_resize(_grid, ds_grid_width(_grid), ds_grid_height(_grid)-1);
	
	// Columns
	var _column_names = ds_map_create();
	var _list_column_names = ds_list_create();
	if (_has_column_names) {
		var _fid = file_text_open_read(_filename);
		var _str = file_text_readln(_fid);
		var _unbalanced_quotes = string_count("\"", _str) % 2;
		if  (_unbalanced_quotes) {
			show_debug_message("[fnc_JBQL_CreateTableCSV] WARNING: First line of CSV "+_filename+" has unbalanced quotation marks - this will possibly create more columns than needed.");
		}
		
		// Load file-provided column names		
		_str = fnc_aux_JBLQ_string_cleanse(_str);		
		_list_column_names = fnc_aux_JBQL_StringToList(_str, ",", true);
		
		file_text_close(_fid);
	}
	else {
		// Load default column names into map		
		for (var _i=0; _i<ds_grid_width(_grid); _i++) {
			ds_list_add(_list_column_names, "Column "+string(_i+1));
			/*ds_map_add(_column_names, "Column "+string(_i+1), _i);*/
		}
	}
	
	// Set up data types
	var _data_types = ds_list_create();
	var _current_row = 0;
	var _num_rows = ds_grid_height(_grid);
	var _num_cols = ds_grid_width(_grid);
	
	for (_i=0; _i<_num_cols; _i++) {
		_data_types[| _i] = JBQL_TYPES.NUM;
	}
	
	var _finished = false;
	while (_current_row < _num_rows && !_finished) {
		_finished = true;
		for (_i=0; _i<_num_cols; _i++) {			
			_data_types[| _i] = _data_types[| _i] && fnc_aux_JBQL_IsNumeric(_grid[# _i,_current_row]);
			_finished = _finished && _data_types[| _i];			
		}
		_current_row++;
	}
	
	var _indices = ds_map_create();
	
	// Set struct	
	var _struct = {
		table_name : _table_name,											// Table name (string)
		column_names_by_position : _list_column_names,						// Column names by position (list)
		column : function(_column_name) {
			return ds_list_find_index(self.column_names_by_position, _column_name);			
		},																	// Column positions by name (function, returns position)		
		column_types : _data_types,											// Column types (list)
		data : _grid,														// Data (grid)
		cell : function(_colname, _row) {									// Cell (using column name and row)
			return self.data[# self.column(_colname), _row];
		},
		num_rows : function() { return ds_grid_height(data); },				// Rows (integer)
		num_columns : function() { return ds_grid_width(data); },			// Columns (integer)
		indices : _indices,													// Indices (map)
		num_indices : function() { return ds_map_size(indices); }			// Number of indices (integer)
	};
	
	return _struct;
}


/// @function		fnc_JBQL_CreateIndex()
/// @description	Creates an index on a table
/// @param			table - The JBQL table (struct)
/// @param			column - The column to index

function fnc_JBQL_CreateIndex() {
	if (argument_count == 0) {		
		throw ("[fnc_JBQL_CreateIndex] JBQL table not provided.");
	}
	if (argument_count == 1) {
		throw ("[fnc_JBQL_CreateIndex] Column name not provided.");
	}
	
	if (argument_count >= 2) {		
		var _struct = argument[0];
		var _column_name = argument[1];
		if (!is_struct(_struct)) {
			throw ("[fnc_JBQL_CreateIndex] Malformed JBQL table.");
		}
		if (_struct.column(_column_name) == -1) {
			throw ("[fnc_JBQL_CreateIndex] Provided column does not exist in JBQL table.");
		}		
	}
	
	var _num_rows = _struct.num_rows();
	show_debug_message(string(_num_rows));
	var _index = ds_map_create();
	for (var _i=0; _i<_num_rows; _i++) {
		_index[? _struct.data[# _struct.column(_column_name), _i]] = _i;
	}
	ds_map_add(_struct.indices, _column_name, _index);
}



/// @function		fnc_JBQL_Query()
/// @description	Queries a single JBQL table and returns a JBQL table as the result.
/// @param			table - The JBQL table (struct) to query
/// @param			fields - Optional list of fields to retrieve (in order). Can have functions.
/// @param			aliases - Optional list of aliases for the returned fields.
/// @param			where - Optional function that returns true for a single record.
/// @param			aggregation functions - Optional list of aggregation functions to compute.
/// @param			limit - Optional row limit for result (default = 0, which means no limit)
/// @return			A JBQL table (struct) with the result.

function fnc_JBQL_Query() {
	var _table = argument[0];
	var _fields = argument[1];
	var _aliases = argument[2];
	var _where = argument[3];
	var _aggregations = argument[4];
	var _limit = argument[5];
	
	// Where
	
	var _where_result = array_create(_table.num_rows(), false);
	var _num_results = 0;
	for (var _i=0; _i<_table.num_rows(); _i++) {
		if (typeof(_where) == "method") {
			var _result = _where(_table, _i);
		}
		else {			
			var _result = script_execute(asset_get_index(_where), _table, _i);
		}
		
		if (_result) {
			_num_results++;
			_where_result[_i] =	true;
		}
	}
	//show_debug_message(string(_fields[1](_table.cell("Column 1",1), _table.cell("Column 1",2))));
	if (array_length(_fields) > 0) {
		show_debug_message("Sum: "+string(_fields[1](_table.cell("Column 1",1), _table.cell("Column 1",2))));
	}
	show_debug_message("Total: "+string(_num_results));
}


/// @function		fnc_aux_JBQL_StringToList()
/// @description	Takes a string with separators and returns a DS list of the separated string
/// @param			string - Source string to split
/// @param			separator - Optional string separator to use. Default: comma
/// @param			remove leading and trailing spaces - Optional boolean to remove leading and trailing spaces on each item of the list. Default: false
/// @param			complete column names - Optional boolean to complete empty names with default column values. Default: true
/// @return			A DS list with the string split into the parts
function fnc_aux_JBQL_StringToList() {
	if (argument_count == 0) {		
		throw ("[fnc_aux_JBQL_StringToList] String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	if (argument_count >= 2) {		
		var _separator = argument[1];
		if (string_length(_separator) != 1) {
			_separator = ",";
		}
	}
	else {
		var _separator = ",";
	}
	if (argument_count >= 3) {
		var _remove_lead_trail_spaces = argument[2];
	}
	else {
		var _remove_lead_trail_spaces = false;
	}
	
	if (argument_count >= 4) {
		var _complete_names = argument[3];
	}
	else {
		var _complete_names = true;
	}
	
	// Process and split
	var _num_columns_default_name = 0;
	var _list = ds_list_create();	
	var _substring = _string;
	var _next_separator = string_pos(_separator, _substring);
	while (_next_separator != 0) {
		var _found = string_copy(_substring, 0, _next_separator-1);
		if (_remove_lead_trail_spaces) {			
			_found = fnc_aux_JBQL_string_lrtrim(_found);
		}
		
		if (string_length(_found) > 0) {
			ds_list_add(_list, _found);		
		}
		else if (_complete_names) {
			_num_columns_default_name++;
			ds_list_add(_list, "Column "+string(_num_columns_default_name));
		}
		_substring = string_copy(_substring, _next_separator+1, string_length(_substring));				
		var _next_separator = string_pos(_separator, _substring);
	}
	ds_list_add(_list,_substring);
	
	return _list;
}


/// @function		fnc_aux_JBQL_IsNumeric()
/// @description	Takes a string and checks if it corresponds to a numeric value
/// @param			string - String to check
/// @return			True or false
function fnc_aux_JBQL_IsNumeric() {
	if (argument_count == 0) {		
		throw ("[fnc_aux_JBQL_IsNumeric] String argument required and not provided.");
	}
	if (argument_count >= 1) {
		var _string = argument[0];
	}
	try {
		var _num = real(_string);
		return true;
	}
	catch (_exception) {
		return false;	
	}
}

function fnc_aux_JBLQ_string_cleanse() {
	var _str = argument[0];
	for (var _j=0; _j<32; _j++) {
		_str = string_replace_all(_str, chr(_j), "");
	}
	return _str;
}

function fnc_aux_JBQL_string_lrtrim() {
	var _str = argument[0];
	var _j=0;
	var _l=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j<_l) {
		_j++;
	}
	_str = string_copy(_str, _j, _l);
			
	var _j=string_length(_str);
	while (string_copy(_str, _j, 1)==" " && _j>=0) {
		_j--;
	}
	_str = string_copy(_str, 0, _j);
	return _str;
}