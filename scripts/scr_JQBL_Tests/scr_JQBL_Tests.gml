// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function scr_JQBL_Tests(){

	/************************* JBQL TESTS   *************************/

	tbl_space = fnc_JBQL_CreateTableCSV("tbl_space", "C:/Users/biyec/AppData/Local/Dungeon_Crawler/Space_Corrected.csv", true);
	fnc_JBQL_CreateIndex(tbl_space, "Location");


	show_debug_message("Columns");
	var _str = "";
	var _n = tbl_space.num_columns();
	for (var _col = 0; _col < _n; _col++) {
		_str = _str + tbl_space.column_names_by_position[|_col];
		if (_col < _n-1) {
			_str = _str + "|";
		}
	}
	show_debug_message(_str);


	show_debug_message("Columns name array");
	var _str = "";
	var _n = tbl_space.num_columns();
	for (var _col = 0; _col < _n; _col++) {
		var _id = tbl_space.column(tbl_space.column_names_by_position[|_col]);
		_str = _str + string(_id);
		if (_col < _n-1) {
			_str = _str + "|";
		}
	}
	show_debug_message(_str);


	show_debug_message("Column Types");
	var _str = "";
	var _n = tbl_space.num_columns();
	for (var _col = 0; _col < _n; _col++) {
		_str = _str + (tbl_space.column_types[|_col] ? "num" : "char");
		if (_col < _n-1) {
			_str = _str + "|";
		}
	}
	show_debug_message(_str);


	for (var _row = 0; _row < 10; _row++;) {
		var _str = "";
		for (var _col = 0; _col < tbl_space.num_columns(); _col++;) {
			_str = _str + string(tbl_space.data[# _col, _row]);
			if (_col < tbl_space.num_columns()-1) {
				_str = _str + "|";
			}
		}
		show_debug_message(_str);
	}

	//show_debug_message(tbl_space.data[# tbl_space.column("Datum"),0]);
	show_debug_message(tbl_space.cell("Datum",0));

	show_debug_message(tbl_space.indices[? "Location"][? "Pad A, Boca Chica, Texas, USA"]);

	test = function(_table, _row) {
		return _table.cell("Company Name", _row) == "SpaceX" && _table.cell("Status Mission", _row) != "Success";
	}
	fnc_JBQL_Query(tbl_space, x, x, test, x, x);
	//fnc_JBQL_Query(tbl_space, x, x, function(_table, _row) { return _table.data[# _table.column("Location"), _row] == "LC-39A, Kennedy Space Center, Florida, USA"; }, x, x);
	fnc_JBQL_Query(tbl_space, x, x, function(_table, _row) { return _table.cell("Location", _row) == "LC-39A, Kennedy Space Center, Florida, USA"; }, x, x);

	//fnc_JBQL_Query(tbl_space, [function(_x) { return floor(_x/30); }, ,function(_x,_y) { return _x-_y;}], , x, x, function(_table, _row) { return _table.cell("Location", _row) == "LC-39A, Kennedy Space Center, Florida, USA"; }, x, x);

	var _identity = function(_x) { return _x; };
	var _sum = function(_x,_y) { return real(_x)+real(_y); };

	fnc_JBQL_Query(	tbl_space,
					[_identity, _sum],
					x,
					function(_table, _row) {
						return _table.cell("Location", _row) == "LC-39A, Kennedy Space Center, Florida, USA"; 
					}, 
					x,
					x);

}