{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 15,
  "bbox_top": 0,
  "bbox_bottom": 15,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 16,
  "height": 16,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"53792e32-1f24-4547-aa69-4c51f4f5d785","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53792e32-1f24-4547-aa69-4c51f4f5d785","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"53792e32-1f24-4547-aa69-4c51f4f5d785","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4f7e0c5-0277-459f-8d26-d232802af880","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4f7e0c5-0277-459f-8d26-d232802af880","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"d4f7e0c5-0277-459f-8d26-d232802af880","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04d651d4-6a2a-4033-b405-3a9b775a6825","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04d651d4-6a2a-4033-b405-3a9b775a6825","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"04d651d4-6a2a-4033-b405-3a9b775a6825","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"81b89641-f197-4530-9e58-8b50876dd334","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"81b89641-f197-4530-9e58-8b50876dd334","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"81b89641-f197-4530-9e58-8b50876dd334","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9152ab9a-9b38-420f-8994-616c7bf9e772","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9152ab9a-9b38-420f-8994-616c7bf9e772","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"9152ab9a-9b38-420f-8994-616c7bf9e772","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5407aa1b-fea7-492b-bd2c-669e7e2b4f17","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5407aa1b-fea7-492b-bd2c-669e7e2b4f17","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"5407aa1b-fea7-492b-bd2c-669e7e2b4f17","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e59d90be-12f2-4a01-a8b1-9e2e87c67ad0","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e59d90be-12f2-4a01-a8b1-9e2e87c67ad0","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"e59d90be-12f2-4a01-a8b1-9e2e87c67ad0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d87a8451-f3a2-4560-9307-d9b6ffbc6a9b","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d87a8451-f3a2-4560-9307-d9b6ffbc6a9b","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"d87a8451-f3a2-4560-9307-d9b6ffbc6a9b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ad60824d-a853-447e-b882-dea6b28de5cd","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ad60824d-a853-447e-b882-dea6b28de5cd","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"ad60824d-a853-447e-b882-dea6b28de5cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62269855-0752-45ec-86d8-15475ac53773","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62269855-0752-45ec-86d8-15475ac53773","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"62269855-0752-45ec-86d8-15475ac53773","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d5d5d3d1-c8b5-4875-b957-2d3f7c9e9d94","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d5d5d3d1-c8b5-4875-b957-2d3f7c9e9d94","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"d5d5d3d1-c8b5-4875-b957-2d3f7c9e9d94","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1fc8fbb6-f30a-4891-b98c-3b7e01cae9d7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1fc8fbb6-f30a-4891-b98c-3b7e01cae9d7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"1fc8fbb6-f30a-4891-b98c-3b7e01cae9d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c96ca64a-f89d-447f-8012-444536aa4dde","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c96ca64a-f89d-447f-8012-444536aa4dde","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"c96ca64a-f89d-447f-8012-444536aa4dde","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9dc52b7f-5779-4064-ab67-47453d4c97a2","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9dc52b7f-5779-4064-ab67-47453d4c97a2","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"9dc52b7f-5779-4064-ab67-47453d4c97a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"554d956e-7c4c-4541-bc1e-398939370b35","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"554d956e-7c4c-4541-bc1e-398939370b35","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"554d956e-7c4c-4541-bc1e-398939370b35","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89b67ee9-8e6c-484b-a80e-12390cbb992d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89b67ee9-8e6c-484b-a80e-12390cbb992d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"89b67ee9-8e6c-484b-a80e-12390cbb992d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4a22a587-3700-4a9c-8989-0ff73291ec85","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a22a587-3700-4a9c-8989-0ff73291ec85","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"4a22a587-3700-4a9c-8989-0ff73291ec85","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8cbfcb46-5606-44c5-910c-5d965b2af51f","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8cbfcb46-5606-44c5-910c-5d965b2af51f","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"8cbfcb46-5606-44c5-910c-5d965b2af51f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee27a5a4-3b2c-437f-b932-e61f01610c34","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee27a5a4-3b2c-437f-b932-e61f01610c34","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"ee27a5a4-3b2c-437f-b932-e61f01610c34","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a07c6db7-7478-42f6-84eb-7c4d248fba8d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a07c6db7-7478-42f6-84eb-7c4d248fba8d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"a07c6db7-7478-42f6-84eb-7c4d248fba8d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2a6bbb22-c645-447c-9058-2044b2f54c73","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2a6bbb22-c645-447c-9058-2044b2f54c73","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"2a6bbb22-c645-447c-9058-2044b2f54c73","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3247c6aa-c43c-491c-90a6-952a62389cb3","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3247c6aa-c43c-491c-90a6-952a62389cb3","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"3247c6aa-c43c-491c-90a6-952a62389cb3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eacb5890-23f3-4b57-8035-f9120d3cd429","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eacb5890-23f3-4b57-8035-f9120d3cd429","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"eacb5890-23f3-4b57-8035-f9120d3cd429","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"36e9ddd0-981f-4826-acc6-44ac867d49c4","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"36e9ddd0-981f-4826-acc6-44ac867d49c4","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"36e9ddd0-981f-4826-acc6-44ac867d49c4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a8e84050-abf2-414a-b69e-451610b06470","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8e84050-abf2-414a-b69e-451610b06470","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"a8e84050-abf2-414a-b69e-451610b06470","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c3a12904-1449-436c-905d-968f3c3e18f7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c3a12904-1449-436c-905d-968f3c3e18f7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"c3a12904-1449-436c-905d-968f3c3e18f7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a85870d7-bdf3-4a70-b180-9700905629d6","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a85870d7-bdf3-4a70-b180-9700905629d6","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"a85870d7-bdf3-4a70-b180-9700905629d6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"34aea64f-f8ba-46d4-b34b-0d2a364217e7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"34aea64f-f8ba-46d4-b34b-0d2a364217e7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"LayerId":{"name":"5352f251-da58-4387-97b1-90195d11d739","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","name":"34aea64f-f8ba-46d4-b34b-0d2a364217e7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 28.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6f023f92-96eb-4557-9797-48049efa37b3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53792e32-1f24-4547-aa69-4c51f4f5d785","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c8d6c5f2-5f4b-4f6e-8091-5b6d82967303","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4f7e0c5-0277-459f-8d26-d232802af880","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"26b54fd6-255b-4aaa-94ca-52cd445541b9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04d651d4-6a2a-4033-b405-3a9b775a6825","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b63efd8e-1f4d-4813-92fd-41b487b4d5f0","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"81b89641-f197-4530-9e58-8b50876dd334","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ab5d44c-a93e-4d43-ae0a-b6cb39117d8b","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9152ab9a-9b38-420f-8994-616c7bf9e772","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6406a547-73f0-4a35-9cce-9c359beedc12","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5407aa1b-fea7-492b-bd2c-669e7e2b4f17","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9108941c-8fc8-42b3-91cf-13986b6ed832","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e59d90be-12f2-4a01-a8b1-9e2e87c67ad0","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a4f8fe20-fe8e-4d5e-bf9e-2725358ba23d","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d87a8451-f3a2-4560-9307-d9b6ffbc6a9b","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1b450e33-0ef7-4993-b6bf-320bd471ad6f","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ad60824d-a853-447e-b882-dea6b28de5cd","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2fe93c1d-4418-4ce8-aaf7-a31dbcd868dc","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62269855-0752-45ec-86d8-15475ac53773","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7f079e60-7601-4c02-bb90-5176a07492e7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d5d5d3d1-c8b5-4875-b957-2d3f7c9e9d94","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c3336993-50aa-4fcb-973e-fb83433c942f","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1fc8fbb6-f30a-4891-b98c-3b7e01cae9d7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"800f777a-4a45-4860-80a1-ec50b74a4596","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c96ca64a-f89d-447f-8012-444536aa4dde","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"829f1ad0-e53f-4726-9dc0-68ea91bdad78","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9dc52b7f-5779-4064-ab67-47453d4c97a2","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f3eafaa8-0c4c-4a11-900c-9895ad611b3e","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"554d956e-7c4c-4541-bc1e-398939370b35","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d7f50d0c-0ce5-413b-bbdb-bd65ea19c701","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89b67ee9-8e6c-484b-a80e-12390cbb992d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4e7e4d48-7007-441e-a192-a362744137a6","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a22a587-3700-4a9c-8989-0ff73291ec85","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"087b732d-3a67-4dc1-ba40-c08582811c8a","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8cbfcb46-5606-44c5-910c-5d965b2af51f","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c2f3bf7e-79dc-4fd0-99ca-b10535a97357","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee27a5a4-3b2c-437f-b932-e61f01610c34","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a1741c31-db6d-4a7a-a650-e8c6cf56b104","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a07c6db7-7478-42f6-84eb-7c4d248fba8d","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"142ca386-ca04-4b08-8839-ef58be334397","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2a6bbb22-c645-447c-9058-2044b2f54c73","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1cf2c39d-37a8-4fa1-85b8-b71d8a8f48b3","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3247c6aa-c43c-491c-90a6-952a62389cb3","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7f2c8c73-d3d3-4212-ae1b-c8017c2db7bc","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eacb5890-23f3-4b57-8035-f9120d3cd429","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"69cc5166-dbb9-4838-8878-e9a88e7745f7","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"36e9ddd0-981f-4826-acc6-44ac867d49c4","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"586cf984-3163-4745-a4ad-93a971293b10","Key":24.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8e84050-abf2-414a-b69e-451610b06470","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"91474ab9-5083-423c-96a4-538a148e9ae1","Key":25.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c3a12904-1449-436c-905d-968f3c3e18f7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6702fed3-62ec-4e70-8426-b28a9323970d","Key":26.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a85870d7-bdf3-4a70-b180-9700905629d6","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00e8f257-9b3f-438d-ba1f-71e11fcfb678","Key":27.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"34aea64f-f8ba-46d4-b34b-0d2a364217e7","path":"sprites/spr_Ghost/spr_Ghost.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 8,
    "yorigin": 8,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Ghost","path":"sprites/spr_Ghost/spr_Ghost.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Ghost",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5352f251-da58-4387-97b1-90195d11d739","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PC and NPC",
    "path": "folders/Sprites/PC and NPC.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Ghost",
  "tags": [],
  "resourceType": "GMSprite",
}